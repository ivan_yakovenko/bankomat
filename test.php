<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 08.11.2017
 * Time: 21:01
 */

require_once "Bankomat.php";

$coins = [
    30 => 10,
    100 => 3,
    400 => 0,
    500 => 2,
    1000 => 5,
];
$number = 400;

$bankomat = new Bankomat($coins);
$result = $bankomat->process($number);


echo "Сумма: $number$" . PHP_EOL;
echo "__________" . PHP_EOL;
if ($result === null) {
    echo "Невозможно разменять";
} else {
    foreach ($result as $value => $count) {
        if ($count === 0) {
            continue;
        }
        echo $value . "$ - " . $count . "шт." .PHP_EOL;
    }
}




