<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 08.11.2017
 * Time: 21:00
 */

class Bankomat
{
    /**
     * @var array
     */
    protected $coins = [];

    /**
     * Bankomat constructor.
     * @param array $coins
     */
    public function __construct(array $coins)
    {
        $this->coins = $coins;
    }


    public function process($sum)
    {
        $count_coins = count($this->coins);
        $array_coins = array_keys($this->coins);
        $count_coins_for_sum = [];

        // Для суммы 0 требуется ноль купюр
        $count_coins_for_sum[0]['count'] = 0;
        $count_coins_for_sum[0]['coins'] = [];
        foreach ($this->coins as $value => $count) {
            $count_coins_for_sum[0]['coins'][$value] = 0;
        }

        for ($tmp_sum = 1; $tmp_sum <= $sum; $tmp_sum++) {
            // Предварительно считаем текущую сумму как невозможную для размена
            $count_coins_for_sum[$tmp_sum]['count'] = PHP_INT_MAX;
            $count_coins_for_sum[$tmp_sum]['coins'] = [];
            // пробегаем все купюры от мала до велика
            for ($i = 0; $i < $count_coins; $i++) {
                $current_coin_value = $array_coins[$i];
                // если купюр больше нет в банкомате

                // если текущая купюра не превышает $tmp_sum
                // и количество купюр превышает более чем на 1 количество купюр от суммы, которая меньше ровно на номинал рассматриваемой купюры
                // то мы текущему количеству присваивам предыдущее + 1
                if ($current_coin_value <= $tmp_sum && $count_coins_for_sum[$tmp_sum - $current_coin_value]['count'] + 1 < $count_coins_for_sum[$tmp_sum]['count']) {

                    // Логика по ограничению числа купюр
                    if (isset($count_coins_for_sum[$tmp_sum - $current_coin_value]['coins'][$current_coin_value])) {
                        if ($count_coins_for_sum[$tmp_sum - $current_coin_value]['coins'][$current_coin_value] + 1 > $this->coins[$current_coin_value]) {
                            continue;
                        }
                    }

                    // Заполняем информацию о количестве купюр для тукущей суммы (используется предшествующая информация)
                    $count_coins_for_sum[$tmp_sum]['count'] = $count_coins_for_sum[$tmp_sum - $current_coin_value]['count'] + 1;
                    $count_coins_for_sum[$tmp_sum]['coins'] = $count_coins_for_sum[$tmp_sum - $current_coin_value]['coins'];
                    if (isset($count_coins_for_sum[$tmp_sum]['coins'][$current_coin_value])) {
                        $count_coins_for_sum[$tmp_sum]['coins'][$current_coin_value]++;
                    } else {
                        $count_coins_for_sum[$tmp_sum]['coins'][$current_coin_value] = 1;
                    }
                }
            }
        }

        //Размен невозможен
        if ($count_coins_for_sum[$sum]['count'] === PHP_INT_MAX) {
            return null;
        } else {
            // TODO можно добавить логику уменьшения количества купюр в автомате после выдачи
            return $count_coins_for_sum[$sum]['coins'];
        }
    }
}